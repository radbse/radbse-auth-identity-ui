import { useEffect } from 'react'
import { useAuthApi } from '@radbse/auth-identity'

const Logout = ({ navigate }) => {
    const [actions, useApi] = useAuthApi()
    const [_, logout] = useApi(actions.api.logout, false)

    useEffect(() => {
        const load = async () => {
            await logout()
            navigate('/')
        }

        load()
    }, [logout, navigate])

    return null
}

export default Logout
