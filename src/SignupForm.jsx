import React, { useCallback, useState } from 'react'
import { Form, Button, Message } from 'semantic-ui-react'
import { useAuthApi } from '@radbse/auth-identity'
import { useForm } from '@radbse/hooks'
import * as styles from './LoginForm.module.css'

const SignupForm = ({ children, onSignup }) => {
    const [actions, useApi] = useAuthApi()
    const [signupLoading, signup] = useApi(actions.api.signup)
    const [error, setError] = useState(null)

    const [fields, form] = useForm({
        fields: [
            { name: 'username', label: 'Username' },
            { name: 'emailaddress', label: 'Email Address' },
            { name: 'password', label: 'Password' },
            { name: 'confirmpassword', label: 'Confirm Password' },
        ],
        submit: async ({ username, emailaddress, password }) => {
            try {
                setError(null)
                await signup({ username, emailaddress, password })
                if (onSignup) onSignup()
            } catch (error) {
                setError(error)
            }
        },
    })

    return (
        <Form
            error
            className={styles.form}
            onSubmit={useCallback(form.submit, [fields.username, fields.password, signup])}
        >
            {form.errors ? <Message error list={Object.values(form.errors)} /> : null}
            {error ? (
                <Message
                    error
                    header={error.message}
                    content={!Array.isArray(error.error) ? error.error : null}
                    list={Array.isArray(error.error) ? error.error : null}
                />
            ) : null}
            <Form.Input {...fields.username} icon="user" />
            <Form.Input {...fields.emailaddress} icon="send" />
            <Form.Input type="password" {...fields.password} icon="lock" />
            <Form.Input type="password" {...fields.confirmpassword} icon="lock" />
            <Button fluid primary content="Sign Up" loading={signupLoading} />

            <div>{children}</div>
        </Form>
    )
}

export default SignupForm
