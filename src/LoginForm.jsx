import React, { useCallback, useState } from 'react'
import { Form, Button, Message } from 'semantic-ui-react'
import { Link } from '@reach/router'
import { useAuthApi } from '@radbse/auth-identity'
import { useForm } from '@radbse/hooks'
import * as styles from './LoginForm.module.css'

const LoginForm = ({ children, onLogin }) => {
    const [actions, useApi] = useAuthApi()
    const [loginLoading, login] = useApi(actions.api.login)
    const [error, setError] = useState(null)

    const [fields, form] = useForm({
        fields: [{ name: 'username', label: 'Username' }, { name: 'password', label: 'Password' }],
        submit: async ({ username, password }) => {
            try {
                setError(null)
                await login({ username, password })
                if (onLogin) onLogin()
            } catch (error) {
                setError(error)
            }
        },
    })

    const handleSubmit = useCallback(form.submit, [fields.username, fields.password, login])

    return (
        <Form error className={styles.form} onSubmit={handleSubmit}>
            {form.errors ? <Message error list={Object.values(form.errors)} /> : null}
            {error ? (
                <Message
                    error
                    header={error.message}
                    content={!Array.isArray(error.error) ? error.error : null}
                    list={Array.isArray(error.error) ? error.error : null}
                />
            ) : null}
            <Form.Input {...fields.username} icon="user" />
            <Form.Input type="password" {...fields.password} icon="lock" />
            <Button fluid primary content="Login" loading={loginLoading} />
            <Link className={styles.link} to="/forgotpassword">
                Forgot password?
            </Link>
            <div>{children}</div>
        </Form>
    )
}

export default LoginForm
